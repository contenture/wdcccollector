<?php

function verbond_image_blok($atts) {
	extract(shortcode_atts(array(
		'id'        => '',
    	'class'     => '',
	    'style'     => '',
	    'text'		=> '',
	    'src'       => '',
	    'alt'       => '',
	    'video'		=> '',
	    'title'		=> '',
	    'color'		=> ''
	), $atts, 'vvv_image_blok'));

	$id             = ( $id             != ''   ) ? 'id="' . esc_attr( $id ) . '"' : '';
	$class          = ( $class          != ''   ) ? 'x-img ' . esc_attr( $class ) : 'x-img';
	$style          = ( $style          != ''   ) ? 'style="' . $style . '"' : '';
	$src            = ( $src            != ''   ) ? $src : 'http://placehold.it/1170x400/';
	$alt            = ( $alt            != ''   ) ? 'alt="' . $alt . '"' : '';
	$video			= ( $video			!= ''	) ? $video : '';
	$title          = ( $title          != ''   ) ? 'title="' . $title . '"' : '';
	$color			= ( $color			!= ''	) ? 'style="background-color: ' . $color . ';"' : 'style="background-color: #fff"';

	if ( is_numeric( $src ) ) {
    	$src_info = wp_get_attachment_image_src( $src, 'full' );
    	$src      = $src_info[0];
  	}

  	$output = '';

	$output .= "<a {$id} class=\"videopop fancybox.iframe\" href=\"{$video}\" {$style}>";
		$output .= "<div class=\"x-column x-md x-1-1 x-overflow-hidden\">";
			$output .= "<div class=\"image-blok-container\">";
				$output .= "<div class=\"witblok\" {$color}></div>";
				$output .= "<div class=\"tekst-blok\">{$text}</div>";
				$output .= "<div class=\"image-container\">";
					$output .= "<img class=\"img-responsive\" src=\"{$src}\" alt=\"{$alt}\" title=\"{$title}\" />";
				$output .= "</div>";
			$output .= "</div>";
		$output .= "</div>";
	$output .= "</a>";

	return $output;
}

add_shortcode('vvv_image_blok', 'verbond_image_blok');

?>