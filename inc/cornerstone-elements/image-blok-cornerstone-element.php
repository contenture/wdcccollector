<?php

class Image_Blok_Element extends Cornerstone_Element_Base {

  public function data() {
    return array(
      'name'        => 'image-blok-element',
      'title'       => __( 'Image blok element', 'my-text-domain' ),
      'section'     => 'information',
      'description' => __( 'Add a image text blok.', 'my-text-domain' ),
      'supports'    => array( 'id', 'class', 'style' ),
      'render'		=> true
    );
  }

  public function controls() {

    $this->addControl(
      'content',
      'editor',
      NULL,
      NULL,
      ''
    );

    $this->addControl(
      'src',
      'image',
      __( 'Src', csl18n() ),
      __( 'Enter your image.', csl18n() ),
      ''
    );

    $this->addControl(
      'alt',
      'text',
      __( 'Alt', csl18n() ),
      __( 'Enter in the alt text for your image', csl18n() ),
      ''
    );

    $this->addControl(
      'title',
      'text',
      __( 'Title', csl18n() ),
      __( 'Enter in the title text for your image', csl18n() ),
      ''
    );

    $this->addControl(
      'video',
      'text',
      __( 'Video', csl18n() ),
      __( 'Enter the video embed url', csl18n() ),
      ''
    );

    $this->addControl(
      'graphic_color',
      'color',
      __( 'Background Color', csl18n() ),
      __( 'Specify the background color of your graphic.', csl18n() ),
      '#ffffff'
    );

  }

  public function render( $atts ) {

    extract( $atts );

    $shortcode = "[vvv_image_blok {$extra} text=\"{$content}\" src=\"{$src}\" alt=\"{$alt}\" title=\"{$title}\" color=\"{$graphic_color}\"]";

    return $shortcode;

  }
}

?>