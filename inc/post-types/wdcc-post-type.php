<?php

// =============================================================================
// INC/POST-TYPES/WDCC-POST-TYPE.PHP
// -----------------------------------------------------------------------------
// Sets up custom post types for WDCC Figures.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. WDCC Custom Post Type
//   02. Add Thumbnails to the Admin Screen
// =============================================================================

// WDCC Custom Post Type
// =============================================================================

function wdcc_type_init() {

  $slug      = 'wdcc';
  $menu_icon = ( floatval( get_bloginfo( 'version' ) ) >= '3.8' ) ? 'dashicons-tag' : NULL;


  //
  // Enable the custom post type.
  //

  $labels = array(
    'name'               => __( 'WDCC', '__wdcc__' ),
    'singular_name'      => __( 'WDCC Item', '__wdcc__' ),
    'add_new'            => __( 'Add New Item', '__wdcc__' ),
    'add_new_item'       => __( 'Add New WDCC Item', '__wdcc__' ),
    'edit_item'          => __( 'Edit WDCC Item', '__wdcc__' ),
    'new_item'           => __( 'Add New WDCC Item', '__wdcc__' ),
    'view_item'          => __( 'View Item', '__wdcc__' ),
    'search_items'       => __( 'Search WDCC', '__wdcc__' ),
    'not_found'          => __( 'No wdcc items found', '__wdcc__' ),
    'not_found_in_trash' => __( 'No wdcc items found in trash', '__wdcc__' )
  );

  $args = array(
    'labels'          => $labels,
    'public'          => true,
    'show_ui'         => true,
    'supports'        => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'author', 'custom-fields', 'revisions' ),
    'capability_type' => 'post',
    'hierarchical'    => false,
    'rewrite'         => array( 'slug' => $slug, 'with_front' => false ),
    'menu_position'   => 5,
    'menu_icon'       => $menu_icon,
    'has_archive'     => true
  );

  $args = apply_filters( 'wdcc_args', $args );

  register_post_type( 'wdcc', $args );


  //
  // Portfolio tags taxonomy.
  //

  $taxonomy_wdcc_tag_labels = array(
    'name'                       => __( 'WDCC Tags', '__wdcc__' ),
    'singular_name'              => __( 'WDCC Tag', '__wdcc__' ),
    'search_items'               => __( 'Search WDCC Tags', '__wdcc__' ),
    'popular_items'              => __( 'Popular WDCC Tags', '__wdcc__' ),
    'all_items'                  => __( 'All WDCC Tags', '__wdcc__' ),
    'parent_item'                => __( 'Parent WDCC Tag', '__wdcc__' ),
    'parent_item_colon'          => __( 'Parent WDCC Tag:', '__wdcc__' ),
    'edit_item'                  => __( 'Edit WDCC Tag', '__wdcc__' ),
    'update_item'                => __( 'Update WDCC Tag', '__wdcc__' ),
    'add_new_item'               => __( 'Add New WDCC Tag', '__wdcc__' ),
    'new_item_name'              => __( 'New WDCC Tag Name', '__wdcc__' ),
    'separate_items_with_commas' => __( 'Separate WDCC tags with commas', '__wdcc__' ),
    'add_or_remove_items'        => __( 'Add or remove WDCC tags', '__wdcc__' ),
    'choose_from_most_used'      => __( 'Choose from the most used WDCC tags', '__wdcc__' ),
    'menu_name'                  => __( 'WDCC Tags', '__wdcc__' )
  );

  $taxonomy_wdcc_tag_args = array(
    'labels'            => $taxonomy_wdcc_tag_labels,
    'public'            => true,
    'show_in_nav_menus' => true,
    'show_ui'           => true,
    'show_tagcloud'     => true,
    'hierarchical'      => false,
    'rewrite'           => array( 'slug' => $slug . '-tag', 'with_front' => false ),
    'show_admin_column' => true,
    'query_var'         => true
  );

  register_taxonomy( 'wdcc-tag', array( 'wdcc' ), $taxonomy_wdcc_tag_args );


  //
  // WDCC categories taxonomy.
  //

  $taxonomy_wdcc_category_labels = array(
    'name'                       => __( 'WDCC Categories', '__wdcc__' ),
    'singular_name'              => __( 'WDCC Category', '__wdcc__' ),
    'search_items'               => __( 'Search WDCC Categories', '__wdcc__' ),
    'popular_items'              => __( 'Popular WDCC Categories', '__wdcc__' ),
    'all_items'                  => __( 'All WDCC Categories', '__wdcc__' ),
    'parent_item'                => __( 'Parent WDCC Category', '__wdcc__' ),
    'parent_item_colon'          => __( 'Parent WDCC Category:', '__wdcc__' ),
    'edit_item'                  => __( 'Edit WDCC Category', '__wdcc__' ),
    'update_item'                => __( 'Update WDCC Category', '__wdcc__' ),
    'add_new_item'               => __( 'Add New WDCC Category', '__wdcc__' ),
    'new_item_name'              => __( 'New WDCC Category Name', '__wdcc__' ),
    'separate_items_with_commas' => __( 'Separate WDCC categories with commas', '__wdcc__' ),
    'add_or_remove_items'        => __( 'Add or remove WDCC categories', '__wdcc__' ),
    'choose_from_most_used'      => __( 'Choose from the most used WDCC categories', '__wdcc__' ),
    'menu_name'                  => __( 'WDCC Categories', '__wdcc__' ),
  );

  $taxonomy_wdcc_category_args = array(
    'labels'            => $taxonomy_wdcc_category_labels,
    'public'            => true,
    'show_in_nav_menus' => true,
    'show_ui'           => true,
    'show_admin_column' => true,
    'show_tagcloud'     => true,
    'hierarchical'      => true,
    'rewrite'           => array( 'slug' => $slug . '-category', 'with_front' => false ),
    'query_var'         => true
  );

  register_taxonomy( 'wdcc-category', array( 'wdcc' ), $taxonomy_wdcc_category_args );


  //
  // Flush rewrite rules if portfolio slug is updated.
  //

  // if ( get_transient( 'x_portfolio_slug_before' ) != get_transient( 'x_portfolio_slug_after' ) ) {
  //  flush_rewrite_rules( false );
  //  delete_transient( 'x_portfolio_slug_before' );
  //  delete_transient( 'x_portfolio_slug_after' );
  // }

}

add_action( 'init', 'wdcc_type_init' );



// Add Thumbnails to the Admin Screen
// =============================================================================

function wdcc_add_thumbnail_column( $columns ) {
  $thumb   = array( 'thumb' => __( 'Thumbnail', '__wdcc__' ) );
  $columns = array_slice( $columns, 0, 2 ) + $thumb + array_slice( $columns, 1 );
  return $columns;
}

function wdcc_add_thumbnail_column_content( $column ) {
  if ( $column == 'thumb' ) {
    echo '<a href="' . get_edit_post_link() . '">' . get_the_post_thumbnail( get_the_ID(), array( 200, 200 ) ) . '</a>';
  }
}

add_filter( 'manage_wdcc_posts_columns', 'wdcc_add_thumbnail_column', 10, 1 );
add_action( 'manage_wdcc_posts_custom_column', 'wdcc_add_thumbnail_column_content', 10, 1 );