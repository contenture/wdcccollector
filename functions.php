<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Custom Post Type
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );

// Custom Post Type
// =============================================================================

require_once(plugin_dir_path(__FILE__) . 'inc/post-types/wdcc-post-type.php');

// Custom Shortcodes
// =============================================================================

// require_once(plugin_dir_path(__FILE__) . 'inc/shortcodes/shortcodes.php');

// Additional Code here
// =============================================================================

function get_custom_category_parents($id, $taxonomy = false, $link = false, $separator = '/', $nicename = false, $visited = array()) {

	if (!($taxonomy && is_taxonomy_hierarchical( $taxonomy )))
		return '';

	$chain = '';
	// $parent = get_category( $id );
	$parent = get_term( $id, $taxonomy);
	if ( is_wp_error( $parent ) )
		return $parent;

	if ( $nicename )
		$name = $parent->slug;
	else
		$name = $parent->name;

	if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
		$visited[] = $parent->parent;
		// $chain .= get_category_parents( $parent->parent, $link, $separator, $nicename, $visited );
		$chain .= get_custom_category_parents( $parent->parent, $taxonomy, $link, $separator, $nicename, $visited );
	}

	if ( $link ) {
		// $chain .= '<a href="' . esc_url( get_category_link( $parent->term_id ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
		$chain .= '<a href="' . esc_url( get_term_link( (int) $parent->term_id, $taxonomy ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
	} else {
		$chain .= $name.$separator;
	}
	return $chain;
}

// Custom Cornerstone Element
// ==========================================================================
// add_action( 'cornerstone_load_elements', 'vvv_image_blok_element' );

// function vvv_image_blok_element() {
//   require_once('inc/cornerstone-elements/image-blok-cornerstone-element.php');
//   cornerstone_add_element('Image_Blok_Element');
// }



